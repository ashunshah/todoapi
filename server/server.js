var express = require('express')
var bodyparser = require('body-parser')

var {mongoose} = require('../db/mongoose')
var {Todo} = require('../models/todo')
var {User} = require('../models/users')

var app = express()

app.use(bodyparser.json());

app.post('/todos',(req,res)=>{
  console.log(req.body);
  var todo = new Todo ({
    text: req.body.text
  });
  todo.save().then((doc)=>{
    res.send(doc);
  },(e) =>{ 
    console.log(e);
    res.status(400).send(e);
  })
});

app.listen(3000,()=>{
  console.log('started on port 3000')
});

