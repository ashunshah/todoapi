var mongoose = require('mongoose');

mongoose.Promise = global.Promise;
mongoose.connect('mongodb://localhost:27017/TodoApp');

var Todo = mongoose.model('Todo',{
    text:{
        type: String
    },
    completed:{
        type: Boolean
    },
    completedAT:{
        type: Number
    }
});

var newTodo = new Todo({
    text:'awesome'
});

newTodo.save().then((doc) =>{
    console.log('saved todo',doc)
},(e)=>{
    console.log('unable to save todo');
});


